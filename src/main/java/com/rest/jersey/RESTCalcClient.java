package com.rest.jersey;

import java.util.Scanner;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class RESTCalcClient {
	static final String REST_URI = "http://localhost:8081/RESTCalc/rest";
	static final String ADD_PATH = "calc/add";
	static final String SUB_PATH = "calc/sub";
	static final String MUL_PATH = "calc/mul";
	static final String DIV_PATH = "calc/div";

	public static void main(String[] args) {

		boolean flag1;
		boolean flag2;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(REST_URI);
		Scanner console = new Scanner(System.in);
		do {
			System.out.println("Please enter the first number");
			int n1 = console.nextInt();
			System.out.println("Please enter the second number");
			int n2 = console.nextInt();

			do {
				System.out.println("which operation do you want to choose?");
				System.out.println("1:addition,2:substraction,3:multiplication,4:division");
				int operation = console.nextInt();
				switch (operation) {
				case 1:
					WebResource addService = service.path(ADD_PATH).path(n1 + "/" + n2);
					System.out.println("Add Response: " + getResponse(addService));
					System.out.println("Add Output as Text: " + getOutputAsText(addService));
					break;
				case 2:
					WebResource subService = service.path(SUB_PATH).path(n1 + "/" + n2);
					System.out.println("Sub Response: " + getResponse(subService));
					System.out.println("Sub Output as Text: " + getOutputAsText(subService));
					break;
				case 3:
					WebResource mulService = service.path(MUL_PATH).path(n1 + "/" + n2);
					System.out.println("Mul Response: " + getResponse(mulService));
					System.out.println("Mul Output as Text: " + getOutputAsText(mulService));
					break;
				case 4:
					WebResource divService = service.path(DIV_PATH).path(n1 + "/" + n2);
					System.out.println("Div Response: " + getResponse(divService));
					System.out.println("Div Output as Text: " + getOutputAsText(divService));
					break;

				}
				System.out.println("Do you want to continue? true or false?");
				flag1 = console.nextBoolean();
			} while (flag1);

			System.out.println("Do you want to do it again? true or false?");
			flag2 = console.nextBoolean();
		} while (flag2);

		console.close();
	}

	private static String getResponse(WebResource service) {
		return service.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class).toString();
	}

	private static String getOutputAsText(WebResource service) {
		return service.accept(MediaType.TEXT_PLAIN).get(String.class);
	}
}
