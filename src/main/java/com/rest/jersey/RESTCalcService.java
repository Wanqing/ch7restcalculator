package com.rest.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/calc")
public class RESTCalcService {

	String returnMsg;

	@GET
	@Path("/{param}")
	public String getMsg(@PathParam("param") String msg) {
		returnMsg = "Hello " + msg;
		return returnMsg;
	}

	@GET
	@Path("/add/{a}/{b}")
	@Produces(MediaType.TEXT_PLAIN)
	public String addPlainText(@PathParam("a") int a, @PathParam("b") int b) {
		returnMsg = "The addition of " + a + " and " + b + " is : " + (a + b);
		return returnMsg;
	}

	@GET
	@Path("/sub/{a}/{b}")
	@Produces(MediaType.TEXT_PLAIN)
	public String subPlainText(@PathParam("a") int a, @PathParam("b") int b) {
		returnMsg = "The subtraction of " + a + " and " + b + " is : " + (a - b);
		return returnMsg;
	}

	@GET
	@Path("/mul/{a}/{b}")
	@Produces(MediaType.TEXT_PLAIN)
	public String mulPlainText(@PathParam("a") int a, @PathParam("b") int b) {
		returnMsg = "The multiplication of " + a + " and " + b + " is : " + (a * b);
		return returnMsg;
	}

	@GET
	@Path("/div/{a}/{b}")
	@Produces(MediaType.TEXT_PLAIN)
	public String divPlainText(@PathParam("a") int a, @PathParam("b") int b) {
		if (b == 0) {
			returnMsg = "Division by 0 is not permitted";
			return returnMsg;
		}
		returnMsg = "The division of " + a + " and " + b + " is : " + (a / b) + " and the Remainder is : " + (a % b);
		return returnMsg;
	}
}

